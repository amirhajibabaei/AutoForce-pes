import numpy as np
from ase.atoms import Atoms
import json


def read_pubchem_json(file):
    with open(file) as f:
        data = json.load(f)
    numbers = data['PC_Compounds'][0]['atoms']['element']
    coords = [data['PC_Compounds'][0]['coords'][0]['conformers'][0][c]
              for c in ('x', 'y', 'z')]
    atoms = Atoms(numbers=numbers, positions=np.array(coords).T)
    return atoms
