### Hydrocarbons



#### Simulation details

* VASP/DFT+D3
* PBC box with 15 angstroms vacuum



#### Models

Elementary molecules:
* Alkane: Methane (1), Ethane (1), Propane (1), Butane (3), Pentane (5), Hexane (10), Heptane (10), Octane (10)
* Alkene: Ethylene (1), Propylene (1), Butene (3), Pentene (8), Hexene (?)
* Alkene-23: (Cis-/Trans-) 2-butene (2), 2-pentene (7), 2-Hexene (4), 3-Hexene (16)
* Alkene-branch: Isobutylene (1), 2-methyl/ethyl-1-butene (2/9), Trans/Cis-3-methyl-2-pentene (4/3), 2,3-Dimethyl-1-butene (3)
* Alkyne: Ethyne (1), Propyne (1), 1-Butyne (1), 2-Butyne (1), 1-Pentyne (3), 2-Pentyne (1), 1-Hexyne (8), 2-Hexyne (3), 3-Hexyne (1)
* Alkadiene: Butadiene (2), Pentadiene (2), 1,5-Hexadiene (10), Heptadiene (10), Octadiene (10)
* Cycloalkane: Cyclopropane (1), Cyclobutane (1), Cyclopentane (1), Cyclohexane (1), Cycloheptane (1), Cyclooctane (2), Cyclononane (6), Cyclodecane (10)
* Cycloalkene: Cyclopropene (1), Cyclobutene (1), Cyclopentene (1), Cyclohexene (1), Cycloheptene (1), Cyclooctene (1:Cis), Trans-cyclooctene (1)
* Isoalkane: Isobutane (1), Isopentane (1), Isohexane (2), Isoheptane (3), Isooctane (1), Isononane (10), Isodecane (10)
* Cycloalkane-methyl: Methyl-/Dimethyl- cyclopropane (1/4), cyclobutane (2/3), cyclopentane (2/9), cyclohexane (4/14), cycloheptane (8/0), cyclooctane (10/30)
* Bicycloalkane: bicyclobutane (3), bicycloheptane (10), bicyclodecane (10), Methylbicyclo-nonane (10), 1-cyclopropyl-1-methylcyclohexane (6), spirodecane (4), bicyclononane (10)

Bulk/intermolecular:
* alkane-bulk [alkane]: dilute gas and liquid Methane, Ethane, Propane, Butane


##### Notes
* Inside parentheses are the n.o. conformers simulated.
* Allene is excluded from Alkadiene (not available in pubchem)
* Isoalkane: Alkane with methyl branches
* Cycloalkane-methyl: Cycloalkane with methyl branches
* [] in front of a model indicates its prerequisite models


#### Test set

```python
from ase.db import connect

db = connect('test.db')                     # NOTE: all keywords are converted to lowercase letters
# data = db.select(group='molecules')       # selects all data
# data = db.select(subgroup='alkane')       # selects all alkanes
data = db.select(molecule='hexane')         # selects all data specifically for Hexane

for row in data:
    atom = row.toatoms()
    energy = atoms.get_potential_energy()
    forces = atoms.get_forces()
    # test here
```
