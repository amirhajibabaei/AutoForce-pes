## ML models for boron nitride (BN)


### Structures

| index | mp-id       | e-hull |
|-------|-------------|--------|
| 0     | mp-984      | 0      | 
| 1     | mp-7991     | 0.00026| 
| 2     | mp-13150    | 0.002  | 
| 3     | mp-629015   | 0.0036 | 
| 4     | mp-604884   | 0.0041 | 
| 5     | mp-685145   | 0.0063 | 
| 6     | mp-1639     | 0.077  | 
| 7     | mp-2653     | 0.095  | 
| 8     | mp-1599     | 0.14   | 
| 9     | mp-13151    | 0.18   | 
| 10    | mp-344      | 0.22   | 
| 11    | mp-644751   | 0.27   | 
| 12    | mp-1077506  | 0.3    | 
| 13    | mp-569655   | 0.32   | 
| 14    | mp-1245301  | 0.62   | 
| 15    | mp-1244872  | 0.64   | 
| 16    | mp-1244943  | 0.66   | 
| 17    | mp-1245231  | 0.66   | 
| 18    | mp-1245228  | 0.67   | 
| 19    | mp-1244991  | 0.74   | 
| 20    | mp-1060281  | 0.81   | 
| 21    | mp-1245193  | 0.82   | 
| 22    | mp-1245297  | 0.83   | 
| 23    | mp-1245281  | 0.84   | 
| 24    | mp-1244917  | 0.92   | 
| 25    | mp-601223   | 2.6    |



### DFT
* VASP: PBE+TS with k-spacing 0.7.
DFT data maybe refined after sampling is complete.



### Models
* BN-alpha: 
Trained with structures 1-13, and 25 at 1000 K.
These resemble simple crystaline structures, 
and except 25, they remained stable during 10 ps of MD.
Structure 25 immidiately transitioned into one of 1-13.

* BN-beta:
Trained with structure 20 at 1000 K.
This structure is made of chains of B-N.
During MD the chains are destroyed and another (random)
structure is formed.

* BN-gamma:
Trained with structure 14 at 1000 K.
This is a complicated structure which contains
N2 molecules in its unit-cell.
The remaining structures (15-24, except 20) are
somewhat similar to 14 and so are not considered
for training.

* BN-delta (liquid phase):
Trained with (melted) structure 6 (cubic) at 6000 K
and 1 bar (NPT-MD).

* BN-zeta (liquid phase):
Trained with (melted) structure 0 (hexagonal) at 
6000 K (NVT-MD).
In this case, nitrogen gas is released during MD.
