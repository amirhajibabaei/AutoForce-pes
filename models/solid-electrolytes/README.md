### Crystals

* LiPS: mp-985583, mp-1097036

* LiGeS: mp-30249, mp-1222529, mp-1222582

* LiGePS: mp-696128, mp-696138


### Details
* NPT-MD: 300~1500 K, 1 bar
